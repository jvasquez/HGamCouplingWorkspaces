Check out a copy of the xml workspace builder and a copy of the 
xml configuration files for the coupling analysis.
```
git clone ssh://git@gitlab.cern.ch:7999/jvasquez/xmlAnaWSBuilder_ICHEP.git xmlAnaWSBuilder
cd xmlAnaWSBuilder/
git clone ssh://git@gitlab.cern.ch:7999/jvasquez/HGamCouplingWorkspaces.git
```

Compile the xml workspace builder and build your workspace
```
source setup.sh
make clean && make
./exe/XMLReader -x HGamCouplingWorkspaces/ICHEP_2016/config/coupling_ICHEP.xml
```

A workspace will be created in ``HGamCouplingWorkspaces/ICHEP_2016/workspace/WS-HGam-Coupling.root``, 
you may edit the xml files to build the workspace as needed. 