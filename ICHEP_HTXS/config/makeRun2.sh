lumi2015='"ATLAS_lumi_run2" Constr="logn" CentralValue="1" Mag="0.021"'
lumi2016='"ATLAS_lumi_run2" Constr="logn" CentralValue="1" Mag="0.029"'
for oldfile in category_HTXS_2015_*.xml; do
  newfile="${oldfile/_2015/_run2}"
  echo "$oldfile --> $newfile"
  cp $oldfile $newfile
  sed -i 's/_2015/_run2/' $newfile
  sed -i 's/3213/13277/' $newfile
  sed -i "s/$lumi2015/$lumi2016/" $newfile
  #sed -i 's/Binning\=\"550\"/Binning\=\"55\"/' $newfile
  #sed -i 's/InjectGhost\=\"1\"/InjectGhost\=\"0\"/' $newfile
done
