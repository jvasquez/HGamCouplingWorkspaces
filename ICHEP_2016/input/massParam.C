#include "RooWorkspace.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooAbsArg.h"
#include "RooRealSumPdf.h"
#include "RooNLLVar.h"
#include "RooMinimizer.h"

#include "RooStats/ModelConfig.h"

void massParam() {
  TFile f("Model_SingleFit_h013_DoubleCB.root");
  RooWorkspace* ws = (RooWorkspace*)(f.Get("signalWS"));
  //ws->Print();
  ws->var("mResonance")->setVal(125.);

  TString t1 ="expr::muCBNom_SM_c%d('@0 + @1 - 125.',muCBNom_SM_m125000_c%d,mResonance)";
  TString t2 ="EDIT::sigPdf_SM_m125000_c%d(sigPdf_SM_m125000_c%d, muCBNom_SM_m125000_c%d=muCBNom_SM_c%d)";

  for( int i(0); i < 13; i++ ) {
    TString newMu = TString::Format(t1.Data(),i,i);
    TString newPdf = TString::Format(t2.Data(),i,i,i,i);
    std::cout << newMu.Data() << std::endl;
    std::cout << newPdf.Data() << std::endl << std::endl;
    ws->factory(newMu);
    ws->factory(newPdf);
  }

  TFile to("MyModel_SingleFit_h013_DoubleCB.root","RECREATE");
  ws->Write("signalWS");
  ws->Print();
}
