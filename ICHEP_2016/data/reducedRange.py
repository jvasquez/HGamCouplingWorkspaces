import os
import ROOT as r

binWidth = 0.1
xmin, xmax = 105., 155.
nBins = int((xmax-xmin)/binWidth)

to = r.TFile("HGamHists.root","RECREATE")
for filename in os.listdir("."):
  if not "data_run2_" in filename: continue
  output = filename.replace("data_run2_","data_reduced_")
  f = open(filename,"r")
  print filename, "-->", output
  o = open(output,"w")
  hname = filename.replace('.txt','')
  h = r.TH1F(hname,hname,nBins,xmin,xmax)
  for l in f:
    n = float(l)
    if (n < xmin): continue
    if (n > xmax): continue
    o.write(str(n)+"\n")
    h.Fill(n)
  h.Write()
